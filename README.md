**A- SLACKBOT**<br>
How to run:<br>
-If you have no web server to run this app make sure to download ngrok.exe(https://ngrok.com/download) file to make your localhost accessible from web<br>
-Run ngrok.exe from cmd line with given http port (ex: ngrok http 5000)(Make sure to close the command line or leave ngrok after you are done with the program)<br>
-Copy forwarding URL from the cmd line to use it later<br>
-Make sure to download your credentials(credentials for ), run the Google Drive API and Google Sheets API from Google Cloud Platform for program to access your google drive files<br>
-Name Oauth2.client credentials as credentials.json and Service account credentials as creds.json - Copy these two files to the same directory with main.py<br>
-Run main.py<br>
-Create and add your bot to a slack channel<br>
-From slack API website create slash commands (Supported commands are: /run_automation, /calculate_budget, /compare campaigns) for your bot and **make sure command parts are equal to slash command names**<br>
(If you are using ngrok paste the URL that is copied to Request URL part and add a text after the URL - ex:https://49f6-94-54-250-25.eu.ngrok.io/**calculate_budget**)<br>
-Click save and run your commands from channel<br>
<br>
Slash Commands;<br>
-/run_automation: this command gets the excel file from directory and creates google sheets files in your google drive<br>
-/calculate_budget: runs with a single paramater that is the required campaign name to calculate the budget for (usage example: /calculate_budget **campaignA**)<br>
-/compare_campaigns: runs with two or more parameters that gets campaing names (make sure to use "-" between parameters ex:/compare_campaigns A-B-C) <br>
					it returns a plotly chart as a .png file to the channel (**Not working right now**)



**B- Suppose that each member of the growth team in Alictus uses the dashboard of company provider X to get
metrics on campaigns and ads. The data/metrics on the dashboard have a tree-like hierarchical structure
and to check the ad metrics one must first log in into the dashboard then list all campaigns, then click the
specific campaign to go to a page that lists target groups, then click on a target group to list all the ads
within that target group then see the metrics for the selected ad. As our games scale, we start noticing that
our members spend a huge amount of time navigating through the dashboard back and forth when all they
need is an all at once dataset in the format header: CampaignName, TargetGroup, Ad, Metric1, Metric2...
Unlike our other partners' company X does not provide an API to retrieve these data automatically. Even
more, company X enforces a user to log out or verify a password every 2 hours and the dashboard becomes
unresponsive on too many requests. Given these constraints how would you design a stable solution to be
able to get these data on a daily granularity? What is the flow of your operations? Could there be any
potential breaking points to your solution? Why/Why not? Can your solution operate on its own as a
background process? Why/Why not?**

I would use a Selenium based script on a webserver to scan the website to collect and upload all the data in the server for better access next time.<br>
This program would run daily and update the database if there is any change in the website data - If holding the data on the server can not be done because of database issues etc. then I would make the script to
hold the URL values for every data point in that way team members can get any data they want by just feeding the script with necessary inputs ( example: to get Metric1 a team member would type - /CampaignName/TargetGroup/Ad/Metric1 )<br>
Since the system does not let users to stay online more than 2 hours an event listener must be in the program to check if the user logged-off by the system if so writing the program to alert user that ran the program to get 
 password info to log-in back to the server would be simple.<br>
To check if the dashboard became unresponsive program can wait before running specific functions or codes - if time is important or the program already runs too slowly 
 we can check how many requests the program is making and adjust it accordingly<br>
These kind of solutions where the websites change constantly or without API assistance would not be a standalone program in my opinion - if this program would run on such a system 
 it would require detailed logging and constant checkings to make sure it runs smoothly<br>
